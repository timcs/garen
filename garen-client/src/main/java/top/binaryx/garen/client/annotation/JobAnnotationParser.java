/*
 * Copyright (c) 2019. https://github.com/timcs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.binaryx.garen.client.annotation;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.aop.framework.Advised;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import top.binaryx.kepler.client.context.JobBeanHolder;
import top.binaryx.kepler.client.handler.JobHandler;
import top.binaryx.kepler.common.exception.KeplerException;

@Slf4j
@Component
public class JobAnnotationParser implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object o, String s) throws BeansException {
        return o;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String s) throws BeansException {
        Object targetBean = getTargetBean(bean);
        Job annotation = targetBean.getClass().getAnnotation(Job.class);
        String jobName;
        if (annotation != null && targetBean instanceof JobHandler) {
            jobName = annotation.name();
            if (StringUtils.isBlank(jobName)) {
                jobName = targetBean.getClass().getSimpleName();
            }

            if (JobBeanHolder.putIfAbsent(jobName, (JobHandler) bean) != null) {
                throw new KeplerException("Duplicated definition job name [" + targetBean.getClass().getName() + "]");
            }
        } else if (targetBean instanceof JobHandler) {
            jobName = targetBean.getClass().getSimpleName();
            if (JobBeanHolder.putIfAbsent(jobName, (JobHandler) bean) != null) {
                throw new KeplerException("Duplicated definition job name [" + targetBean.getClass().getSimpleName() + "]");
            }
        } else if (annotation != null) {
            // TODO 时间紧，这个版本@Job须实现JobHandler，以后可以用静态代理搞定
            throw new KeplerException("This class must be implements JobHandler interface in current version. class [" + targetBean.getClass().getName() + "]");
        }
        return bean;
    }

    private Object getTargetBean(Object bean) {
        if (bean instanceof Advised) {
            try {
                return ((Advised) bean).getTargetSource().getTarget();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
        return bean;
    }
}
