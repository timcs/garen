package top.binaryx.garen.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import top.binaryx.garen.common.handler.EventHandler;
import top.binaryx.garen.client.component.SpringContextHolder;
import top.binaryx.kepler.client.handler.EventHandlerHolder;
import top.binaryx.kepler.client.service.ClientNetworkService;

import java.util.Map;

@Component
public class GarenClientBootstrap implements CommandLineRunner {

    @Autowired
    private ClientNetworkService clientNetworkService;

    @Override
    public void run(String... strings) {
        // 初始化事件处理器
        Map<String, EventHandler> result = SpringContextHolder.getContext()
                .getBeansOfType(EventHandler.class);
        for (Map.Entry<String, EventHandler> entry : result.entrySet()) {
            EventHandler value = entry.getValue();
            EventHandlerHolder.put(value.type(), value);
        }

        // 启动客户端网络服务
        clientNetworkService.init();
    }
}
