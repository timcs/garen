package top.binaryx.garen.server.pojo.dto;

import lombok.Data;


@Data
public class NotExecutedJobResponse {
    private Integer count;
}
