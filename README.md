# garen
Garen a high-performance, java based open source distributed scheduling framework.  

##roadmap  
- [x] leader 未完全迁移导致的job假接管  
- [x] 任务触发，暂停，恢复
- [x] http异步任务
- [x] 重复检测
- [x] 漏跑检测
- [x] demo模块
- [ ] 失败告警
- [ ] 流程任务
- [ ] client模式
- [ ] http任务请求和响应适配问题  