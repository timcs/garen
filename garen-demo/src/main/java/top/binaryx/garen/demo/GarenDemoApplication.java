package top.binaryx.garen.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <>
 *
 * @author hongtian.wei
 * @date 2022-01-07 16:56
 * @since
 */
@SpringBootApplication
public class GarenDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(GarenDemoApplication.class);
    }
}
