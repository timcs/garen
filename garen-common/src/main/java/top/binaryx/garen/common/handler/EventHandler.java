/*
 * Copyright (c) 2019. https://github.com/timcs
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.binaryx.garen.common.handler;

public interface EventHandler<T> {

    /**
     * 处理 接收到的消息.
     *
     * @param msg 消息
     */
    void handle(T msg);

    /**
     * 获取处理类型
     *
     * @return 处理类型
     */
    String type();
}

